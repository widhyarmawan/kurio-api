<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'articles'], function(){
    Route::get('', 'ArticleController@index');
    Route::get('{uuid}', 'ArticleController@show');
    Route::post('', 'ArticleController@addOrUpdate');
    Route::delete('{uuid}', 'ArticleController@delete');
    Route::post('multiple-delete', 'ArticleController@multipleDelete');
});
