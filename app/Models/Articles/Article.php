<?php

namespace App\Models\Articles;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
	protected $table = 'articles';

	protected $guarded = ['id'];

    protected $primaryKey = 'id';

}
