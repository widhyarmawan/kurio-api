<?php

namespace App\Models\Articles;

class ArticleRepository
{
    public function checkArticleByUuid($uuid)
    {
        return Article::where('uuid', $uuid)
                      ->first();
    }

    public function getArticles()
    {
        return Article::get();
    }

    public function createArticle($input)
    {
        return Article::create($input);
    }

    public function updateArticle(Article $article, $input)
    {
        return $article->update($input);
    }

    public function deleteArticle(Article $article)
    {
        return $article->delete();
    }
}
