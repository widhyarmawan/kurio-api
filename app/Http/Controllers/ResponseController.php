<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ResponseController extends Controller
{
    public function json($status_code, $message, $content = [])
	{
		return response()->json(['status_code' => $status_code,
                                 'message'     => $message,
                                 'contents'    => $content]);
	}
}
