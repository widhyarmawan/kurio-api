<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Articles\ArticleRepository as Article;
use App\Http\Controllers\ResponseController as Response;

use Ramsey\Uuid\Uuid;

class ArticleController extends Controller
{
    public function __construct(Article $articles, Response $responses)
    {
        $this->articles  = $articles;
        $this->responses = $responses;
    }

    public function index()
    {
        $articles = $this->articles->getArticles();

        $output['articles'] = $articles;
        return $this->responses->json(200, 'success', $output);
    }

    public function addOrUpdate(Request $request)
    {
        $type         = $request->input('type');
        $article_uuid = $request->input('article_uuid');
        $title        = $request->input('title');
        $content      = $request->input('content');

        $is_image = false;
        if(!empty($_FILES['file'])){
            $is_image = true;
            $file_name = pathinfo($_FILES['file']['name'],PATHINFO_EXTENSION);
            $image = time().'.'.$file_name;
            move_uploaded_file($_FILES["file"]["tmp_name"], 'images/'.$image);
        }

        if ($type === 'new'){
            $inputData = [
                'uuid'    => Uuid::uuid4(),
                'title'   => $title,
                'content' => $content,
                'image'   => $image,
            ];
            $article = $this->articles->createArticle($inputData);
        } else {
            $article = $this->articles->checkArticleByUuid($article_uuid);
            if (empty($article)){
                return $this->responses->json(404, 'Article not found');
            }
            $inputData['title'] = $title;
            $inputData['content'] = $content;
            if ($is_image){
                $inputData['image'] = $image;
            }
            
            $update = $this->articles->updateArticle($article, $inputData);
            if ($update){
                $article = $this->articles->checkArticleByUuid($article_uuid);
            } else {
                return $this->responses->json(500, 'Internal server error');
            }
        }

        $output['article'] = $article;
        return $this->responses->json(200, 'success', $output);
    }

    public function show($uuid)
    {
        $article = $this->articles->checkArticleByUuid($uuid);
        if (empty($article)){
            return $this->responses->json(404, 'Article not found');
        }

        $output['article'] = $article;
        return $this->responses->json(200, 'success', $output);
    }

    public function delete($uuid)
    {
        $article = $this->articles->checkArticleByUuid($uuid);
        if (empty($article)){
            return $this->responses->json(404, 'Article not found');
        }

        $delete = $this->articles->deleteArticle($article);

        if ($delete){
            return $this->responses->json(200, 'success');
        } else {
            return $this->responses->json(500, 'Internal server error');
        }
    }

    public function multipleDelete(Request $request)
    {
        $article_uuids = $request->input('article_uuids');
        if (count($article_uuids) > 0){
            foreach ($article_uuids as $uuid){
                $article = $this->articles->checkArticleByUuid($uuid);
                if ($article){
                    $delete = $this->articles->deleteArticle($article);
                }
            }
        }

        $articles = $this->articles->getArticles();

        $output['articles'] = $articles;
        return $this->responses->json(200, 'success', $output);
    }
}
