# kurio-api [backend] Installation

This is Documentation for Installation tapi by Cindy Yolanda Octavia.

This is built on Laravel Framework 5.2.

## Installation

Clone the repository-
```
git clone https://gitlab.com/widhyarmawan/kurio-api.git kurio-api
```

Then do a composer update
```
composer update
```

Then create a database named `kurio` and then do a database migration using this command-
```
php artisan migrate
```

## Run server

Run server using this command-
```
php artisan serve
```

Then go to `http://localhost:8000` from your browser and see the app.